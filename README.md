# TestGraph

Simple Graph gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'test_graph', git: 'https://gitlab.com/Sandor1924/test_graph.git'
```

And then execute:

    $ bundle install

## Usage

```
graph = TestGraph::Undirected.new
# or graph = TestGraph::Directed.new

graph.add_vertex(1)
graph.add_vertex(2)
graph.add_vertex(3)
graph.add_vertex(4)

graph.add_edge(1, 2)
graph.add_edge(1, 3)
graph.add_edge(3, 4)

graph.get_path(1, 4)
```
