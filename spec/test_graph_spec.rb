RSpec.describe TestGraph do
  it 'has a version number' do
    expect(TestGraph::VERSION).not_to be nil
  end

  context 'TestGraph::Undirected' do
    let(:graph) { TestGraph::Undirected.new }

    it 'can be initialized' do
      expect(graph).to be_instance_of TestGraph::Undirected
    end

    it 'has vertices attribute' do
      expect(graph).to respond_to(:vertices)
    end

    it 'has edges attribute' do
      expect(graph).to respond_to(:edges)
    end

    context 'add_vertex' do
      it 'returns vertex' do
        expect(graph.add_vertex(3)).to be_instance_of TestGraph::Vertex
      end

      it 'adds vertex to graph' do
        vertex = graph.add_vertex(3)

        expect(graph.vertices[3]).to eq vertex
      end

      it 'raises error if vertex already in graph' do
        graph.add_vertex(3)

        expect { graph.add_vertex(3) }
          .to raise_error TestGraph::VertexAlreadyExistsError, 'Vertex 3 already exists'
      end
    end

    context 'add_edge' do
      it 'returns nil' do
        graph.add_vertex(1)
        graph.add_vertex(2)

        expect(graph.add_edge(1, 2)).to eq nil
      end

      it 'adds 2 edges to graph' do
        graph.add_vertex(1)
        graph.add_vertex(2)

        expect { graph.add_edge(1, 2) }.to change { graph.edges.size }.by(2)
      end

      it 'raises error if some of the given vertices does not exist' do
        graph.add_vertex(2)

        expect { graph.add_edge(1, 2) }
          .to raise_error TestGraph::VertexNotFoundError, 'Vertex 1 not found'
      end
    end

    context 'get_path' do
      before do
        graph.add_vertex(1)
        graph.add_vertex(2)
        graph.add_vertex(3)
        graph.add_vertex(4)
        graph.add_vertex(5)

        graph.add_edge(1, 2)
        graph.add_edge(1, 3)
        graph.add_edge(3, 4)
      end

      it 'returns empty array if source == target' do
        expect(graph.get_path(1, 1)).to eq []
      end

      it 'returns array of edges' do
        expect(graph.get_path(1, 4)).to eq [graph.edges.select { |e| e.source == 1 && e.target == 3 },
                                            graph.edges.select { |e| e.source == 3 && e.target == 4 }]
      end

      it 'finds a path in both directions' do
        expect(graph.get_path(4, 1)).to eq [graph.edges.select { |e| e.source == 4 && e.target == 3 },
                                            graph.edges.select { |e| e.source == 3 && e.target == 1 }]
      end

      it 'raises error if some of the given vertices does not exist' do
        expect { graph.get_path(1, 7) }
          .to raise_error TestGraph::VertexNotFoundError, 'Vertex 7 not found'
      end

      it 'raises error if could not find a path' do
        expect { graph.get_path(1, 5) }
          .to raise_error TestGraph::PathNotFoundError, 'Path from 1 to 5 not found'
      end
    end
  end

  context 'TestGraph::Undirected' do
    let(:graph) { TestGraph::Directed.new }

    it 'can be initialized' do
      expect(graph).to be_instance_of TestGraph::Directed
    end

    it 'has vertices attribute' do
      expect(graph).to respond_to(:vertices)
    end

    it 'has edges attribute' do
      expect(graph).to respond_to(:edges)
    end

    context 'add_vertex' do
      it 'returns vertex' do
        expect(graph.add_vertex(3)).to be_instance_of TestGraph::Vertex
      end

      it 'adds vertex to graph' do
        vertex = graph.add_vertex(3)

        expect(graph.vertices[3]).to eq vertex
      end

      it 'raises error if vertex already in graph' do
        graph.add_vertex(3)

        expect { graph.add_vertex(3) }
          .to raise_error TestGraph::VertexAlreadyExistsError, 'Vertex 3 already exists'
      end
    end

    context 'add_edge' do
      it 'returns nil' do
        graph.add_vertex(1)
        graph.add_vertex(2)

        expect(graph.add_edge(1, 2)).to eq nil
      end

      it 'adds 1 edge to graph' do
        graph.add_vertex(1)
        graph.add_vertex(2)

        expect { graph.add_edge(1, 2) }.to change { graph.edges.size }.by(1)
      end

      it 'allows to add a reversed edge' do
        graph.add_vertex(1)
        graph.add_vertex(2)

        graph.add_edge(1, 2)

        expect { graph.add_edge(2, 1) }.to change { graph.edges.size }.by(1)
      end

      it 'raises error if some of the given vertices does not exist' do
        graph.add_vertex(2)

        expect { graph.add_edge(1, 2) }
          .to raise_error TestGraph::VertexNotFoundError, 'Vertex 1 not found'
      end
    end

    context 'get_path' do
      before do
        graph.add_vertex(1)
        graph.add_vertex(2)
        graph.add_vertex(3)
        graph.add_vertex(4)
        graph.add_vertex(5)

        graph.add_edge(1, 2)
        graph.add_edge(1, 3)
        graph.add_edge(3, 4)
      end

      it 'returns empty array if source == target' do
        expect(graph.get_path(1, 1)).to eq []
      end

      it 'returns array of edges' do
        expect(graph.get_path(1, 4)).to eq [graph.edges.select { |e| e.source == 1 && e.target == 3 },
                                            graph.edges.select { |e| e.source == 3 && e.target == 4 }]
      end

      it 'raises error if could not find a path because of the edges directions' do
        expect { graph.get_path(4, 1) }
          .to raise_error TestGraph::PathNotFoundError, 'Path from 4 to 1 not found'
      end

      it 'raises error if some of the given vertices does not exist' do
        expect { graph.get_path(1, 7) }
          .to raise_error TestGraph::VertexNotFoundError, 'Vertex 7 not found'
      end

      it 'raises error if could not find a path' do
        expect { graph.get_path(1, 5) }
          .to raise_error TestGraph::PathNotFoundError, 'Path from 1 to 5 not found'
      end
    end
  end
end
