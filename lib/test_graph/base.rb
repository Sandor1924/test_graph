module TestGraph
  class Base
    attr_accessor :vertices, :edges

    def initialize
      @vertices = {}
      @edges = []
    end

    def add_vertex(value)
      raise VertexAlreadyExistsError.new(value) if @vertices[value]

      @vertices[value] = Vertex.new(value)
    end

    def add_edge(source, target)
      raise NotImplementedError
    end

    def get_path(source, target)
      raise NotImplementedError
    end

    private

    def find_vertex(value)
      return @vertices[value] unless @vertices[value].nil?

      raise VertexNotFoundError.new(value)
    end

    def neighbors(vertex)
      @edges.select { |e| e.source == vertex }.map(&:target).uniq
    end

    def make_path(vertices)
      pairs = vertices.each_with_index.map { |v, i| [v, vertices[i + 1]] }[0..-2]

      pairs.map { |p| edges.select { |e| e.source == p[0] && e.target == p[1] } }
    end
  end

  module Pathfinder
    def get_path(source, target)
      source_vertex = find_vertex(source)
      target_vertex = find_vertex(target)

      return [] if source_vertex == target_vertex

      queue = Queue.new
      queue << [source, [source]]
      visited_vertices = {}

      while !queue.empty? do
        current, path = queue.pop
        visited_vertices[current] = path

        next_vertices = neighbors(current)

        return make_path(path.push(target)) if next_vertices.include? target

        (next_vertices - visited_vertices.keys).each { |v| queue << [v, path + [v]] }
      end

      raise PathNotFoundError.new(source, target)
    end
  end
end
