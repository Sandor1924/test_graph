module TestGraph
  class Edge
    attr_accessor :source, :target

    def initialize(source, target)
      @source = source
      @target = target
    end

    def to_s
      "(#{source} - #{target})"
    end
  end
end
