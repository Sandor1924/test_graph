module TestGraph
  class Directed < Base
    include Pathfinder

    def add_edge(source, target)
      find_vertex(source)
      find_vertex(target)

      @edges << Edge.new(source, target)

      nil
    end
  end
end
