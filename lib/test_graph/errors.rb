module TestGraph
  class VertexAlreadyExistsError < StandardError
    def initialize(value)
      message = "Vertex #{value} already exists"
      super(message)
    end
  end

  class VertexNotFoundError < StandardError
    def initialize(value)
      message = "Vertex #{value} not found"
      super(message)
    end
  end

  class PathNotFoundError < StandardError
    def initialize(source, target)
      message = "Path from #{source} to #{target} not found"
      super(message)
    end
  end
end
