module TestGraph
  class Undirected < Base
    include Pathfinder

    def add_edge(source, target)
      find_vertex(source)
      find_vertex(target)

      @edges << Edge.new(source, target)
      @edges << Edge.new(target, source)

      nil
    end
  end
end
