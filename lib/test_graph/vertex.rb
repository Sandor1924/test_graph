module TestGraph
  class Vertex
    attr_accessor :value

    def initialize(value)
      @value = value
    end
  end
end
